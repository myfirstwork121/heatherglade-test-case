<?php declare(strict_types=1);

require_once dirname(__DIR__) . '/vendor/autoload.php';

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Symfony\Component\Console\Helper\HelperSet;

$dbParams = include 'migrations-db.php';

try {
    $connection = DriverManager::getConnection($dbParams);
} catch (DBALException $e) {
    var_dump($e->getMessage());
}

return new HelperSet([
    'db' => new ConnectionHelper($connection),
]);
