<?php

use Symfony\Component\Routing\Matcher\Dumper\PhpMatcherTrait;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcApp_KernelDevDebugContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    use PhpMatcherTrait;

    public function __construct(RequestContext $context)
    {
        $this->context = $context;
        $this->staticRoutes = [
            '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
            '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
            '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
            '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
            '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
            '/api/v1' => [[['_route' => 'api_index', '_controller' => 'App\\Controller\\DefaultController::index'], null, ['GET' => 0], null, true, false, null]],
            '/api/v1/players' => [[['_route' => 'api_players', '_controller' => 'App\\Controller\\PlayerController::index'], null, ['GET' => 0], null, false, false, null]],
            '/' => [[['_route' => 'index', '_controller' => 'App\\Controller\\DefaultController::index'], null, ['GET' => 0], null, false, false, null]],
        ];
        $this->regexpList = [
            0 => '{^(?'
                    .'|/_(?'
                        .'|wdt/([^/]++)(*:24)'
                        .'|profiler/([^/]++)(?'
                            .'|/(?'
                                .'|search/results(*:69)'
                                .'|router(*:82)'
                                .'|exception(?'
                                    .'|(*:101)'
                                    .'|\\.css(*:114)'
                                .')'
                            .')'
                            .'|(*:124)'
                        .')'
                    .')'
                    .'|/api/v1/players/([^/]++)/monster(?'
                        .'|s(*:170)'
                        .'|(*:178)'
                    .')'
                .')/?$}sD',
        ];
        $this->dynamicRoutes = [
            24 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
            69 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
            82 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
            101 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception::showAction'], ['token'], null, null, false, false, null]],
            114 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception::cssAction'], ['token'], null, null, false, false, null]],
            124 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
            170 => [[['_route' => 'api_players_monsters', '_controller' => 'App\\Controller\\PlayerController::receiveMonsters'], ['player'], ['GET' => 0], null, false, false, null]],
            178 => [[['_route' => 'api_player_open_monsters', '_controller' => 'App\\Controller\\PlayerController::openMonster'], ['player'], ['POST' => 0], null, false, false, null]],
        ];
    }
}
