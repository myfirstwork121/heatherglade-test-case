<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.php */
class __TwigTemplate_9a3e0f2d5e9aa921515221a99bcc5488241e3eb4fd50253a53b488d35e7723dc extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "index.php"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "index.php"));

        // line 1
        echo "<?php declare(strict_types=1);

use App\\Kernel;
use Symfony\\Component\\Debug\\Debug;
use Symfony\\Component\\Dotenv\\Dotenv;
use Symfony\\Component\\HttpFoundation\\Request;

require __DIR__ . '/../vendor/autoload.php';

if (!isset(\$_SERVER['APP_ENV'])) {
    if (!class_exists(Dotenv::class)) {
        throw new RuntimeException(
            'APP_ENV environment variable is not defined. You need to define environment variables 
            for configuration or add \"symfony/dotenv\" as a Composer dependency to load variables from a .env file.'
        );
    }
    (new Dotenv())->load(__DIR__ . '/../.env');
}

if (\$_SERVER['APP_DEBUG'] ?? ('prod' !== (\$_SERVER['APP_ENV'] ?? 'dev'))) {
    umask(0000);

    Debug::enable();
}

\$kernel = new Kernel(
    \$_SERVER['APP_ENV'] ?? 'dev',
    \$_SERVER['APP_DEBUG'] ?? ('prod' !== (\$_SERVER['APP_ENV'] ?? 'dev'))
);

\$request = Request::createFromGlobals();
\$response = \$kernel->handle(\$request);
\$response->send();
\$kernel->terminate(\$request, \$response);
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "index.php";
    }

    public function getDebugInfo()
    {
        return array (  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<?php declare(strict_types=1);

use App\\Kernel;
use Symfony\\Component\\Debug\\Debug;
use Symfony\\Component\\Dotenv\\Dotenv;
use Symfony\\Component\\HttpFoundation\\Request;

require __DIR__ . '/../vendor/autoload.php';

if (!isset(\$_SERVER['APP_ENV'])) {
    if (!class_exists(Dotenv::class)) {
        throw new RuntimeException(
            'APP_ENV environment variable is not defined. You need to define environment variables 
            for configuration or add \"symfony/dotenv\" as a Composer dependency to load variables from a .env file.'
        );
    }
    (new Dotenv())->load(__DIR__ . '/../.env');
}

if (\$_SERVER['APP_DEBUG'] ?? ('prod' !== (\$_SERVER['APP_ENV'] ?? 'dev'))) {
    umask(0000);

    Debug::enable();
}

\$kernel = new Kernel(
    \$_SERVER['APP_ENV'] ?? 'dev',
    \$_SERVER['APP_DEBUG'] ?? ('prod' !== (\$_SERVER['APP_ENV'] ?? 'dev'))
);

\$request = Request::createFromGlobals();
\$response = \$kernel->handle(\$request);
\$response->send();
\$kernel->terminate(\$request, \$response);
", "index.php", "/home/max/Projects/Heatherglade/public/index.php");
    }
}
