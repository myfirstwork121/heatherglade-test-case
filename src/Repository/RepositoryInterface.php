<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\EntityInterface;

/**
 * Interface RepositoryInterface
 * @package App\Repository
 */
interface RepositoryInterface
{
    public const COLUMN_ID = 'id';
    public const COLUMN_NAME = 'name';
    public const COLUMN_MONGO_ID = '_id';


    /**
     * @return EntityInterface[]|null
     */
    public function fetchAll(): ?array;

    /**
     * @param int $id
     * @return EntityInterface|null
     */
    public function fetch(int $id): ?EntityInterface;

    /**
     * @param array $criteria
     * @return EntityInterface[]|null
     */
    public function fetchBy(array $criteria): ?array;
}
