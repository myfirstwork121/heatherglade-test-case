<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\Player;
use App\Helper\Database;
use PDO;

/**
 * Class PlayerRepository
 * @package App\Repository
 */
class PlayerSqlRepository extends AbstractSqlRepository
{
    private const TABLE_NAME = 'players';
    private const RELATION_TABLE = 'players_monsters';
    private const RELATION_TABLE_COLUMN_PLAYER_ID = 'player_id';
    private const RELATION_TABLE_COLUMN_MONSTER_ID = 'monster_id';

    /**
     * PlayerRepository constructor.
     * @param Database $database
     * @param Player $entity
     */
    public function __construct(Database $database, Player $entity)
    {
        parent::__construct($database, $entity, self::TABLE_NAME);
    }

    /**
     * @param int $playerId
     * @param int $monsterId
     * @return bool
     */
    public function addMonster(int $playerId, int $monsterId): bool
    {
        $query = sprintf(
            'INSERT INTO %s (%s, %s) VALUES (:player_id, :monster_id)',
            self::RELATION_TABLE,
            self::RELATION_TABLE_COLUMN_PLAYER_ID,
            self::RELATION_TABLE_COLUMN_MONSTER_ID
        );

        $statement = $this->getConnection()->prepare($query);
        $statement->bindValue(self::RELATION_TABLE_COLUMN_PLAYER_ID, $playerId);
        $statement->bindValue(self::RELATION_TABLE_COLUMN_MONSTER_ID, $monsterId);

        return $statement->execute();
    }

    /**
     * @param int $playerId
     * @param int $monsterId
     * @return array|null
     */
    public function findRelation(int $playerId, int $monsterId): ?array
    {
        $query = sprintf(
            'SELECT * FROM %s WHERE %s = :player_id AND %s = :monster_id',
            self::RELATION_TABLE,
            self::RELATION_TABLE_COLUMN_PLAYER_ID,
            self::RELATION_TABLE_COLUMN_MONSTER_ID
        );

        $statement = $this->getConnection()->prepare($query);
        $statement->bindValue(self::RELATION_TABLE_COLUMN_PLAYER_ID, $playerId, PDO::PARAM_INT);
        $statement->bindValue(self::RELATION_TABLE_COLUMN_MONSTER_ID, $monsterId, PDO::PARAM_INT);
        $statement->execute();

        $result =  $statement->fetch();
        if (!$result) {
            return null;
        }

        return $result;
    }
}
