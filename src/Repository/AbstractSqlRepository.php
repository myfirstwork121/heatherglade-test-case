<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\EntityInterface;
use App\Helper\Database;
use PDO;

/**
 * Class AbstractRepository
 * @package App\Repository
 */
abstract class AbstractSqlRepository implements RepositoryInterface
{
    private const CONCEPT_KEY = 'concept';

    /**
     * @var Database $connection
     */
    protected $database;

    /**
     * @var EntityInterface $entity
     */
    protected $entity;

    /**
     * @var string $tableName
     */
    protected $tableName;

    /**
     * AbstractSqlRepository constructor.
     * @param Database $database
     * @param EntityInterface $entity
     * @param string $tableName
     */
    protected function __construct(
        Database $database,
        EntityInterface $entity,
        string $tableName
    ) {
        $this->database = $database;
        $this->entity = $entity;
        $this->tableName = $tableName;
    }

    /**
     * @return EntityInterface[]|null
     */
    public function fetchAll(): ?array
    {
        $query = sprintf('SELECT * FROM %s', $this->tableName);
        $statement = $this->getConnection()->query($query);

        return $statement->fetchAll(PDO::FETCH_CLASS, $this->entity->getClassName());
    }

    /**
     * @param int $id
     * @return EntityInterface
     */
    public function fetch(int $id): ?EntityInterface
    {
        $query = sprintf('SELECT * FROM %s WHERE %s = :id', $this->tableName, self::COLUMN_ID);
        $statement = $this->getConnection()->prepare($query);
        $statement->bindValue(self::COLUMN_ID, $id, PDO::PARAM_INT);
        $statement->execute();

        $result =  $statement->fetchObject($this->entity->getClassName());
        if (!$result) {
            return null;
        }

        return $result;
    }

    /**
     * @param array $criteria
     * @return EntityInterface[]|null
     */
    public function fetchBy(array $criteria): ?array
    {
        $key = (string)array_key_first($criteria);
        $query = sprintf('SELECT * FROM %s WHERE %s = :concept', $this->tableName, $key);
        $statement = $this->getConnection()->prepare($query);
        $statement->bindValue(self::CONCEPT_KEY, array_shift($criteria));
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_CLASS, $this->entity->getClassName());
    }

    /**
     * @return PDO
     */
    protected function getConnection(): PDO
    {
        return $this->database->openSqlConnection();
    }

    /**
     * AbstractRepository destructor.
     */
    protected function __destruct()
    {
        $this->database->closeSqlConnection();
    }
}
