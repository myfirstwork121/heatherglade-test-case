<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\Monster;
use App\Helper\Database;
use PDO;

/**
 * Class MonsterRepository
 * @package App\Repository
 */
class MonsterSqlRepository extends AbstractSqlRepository
{
    private const TABLE_NAME = 'monsters';

    /**
     * MonsterRepository constructor.
     * @param Database $database
     * @param Monster $entity
     */
    public function __construct(Database $database, Monster $entity)
    {
        parent::__construct($database, $entity, self::TABLE_NAME);
    }

    /**
     * @param int $playerId
     * @return array
     */
    public function fetchMonsters(int $playerId): array
    {
        $query = sprintf(
            'SELECT m.id, m.name FROM %s m JOIN players_monsters pm on m.id = pm.monster_id LEFT JOIN players 
                    p on pm.player_id = p.id WHERE p.id = :id;',
            $this->tableName
        );

        $statement = $this->getConnection()->prepare($query);
        $statement->bindValue(self::COLUMN_ID, $playerId);
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_CLASS, $this->entity->getClassName());
    }
}
