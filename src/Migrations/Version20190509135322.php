<?php declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Schema\SchemaException;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20190509135322
 * @package App\Migrations
 */
final class Version20190509135322 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $table = $schema->createTable('players_monsters');
        $table->addColumn('id', 'integer', ['autoincrement' => true,]);
        $table->addColumn('player_id', 'integer', ['notnull' => true]);
        $table->addForeignKeyConstraint(
            'players',
            ['player_id'],
            ['id'],
            ['onUpdate' => 'CASCADE', 'onDelete' => 'CASCADE'],
            'fk_player_id'
        );
        $table->addColumn('monster_id', 'integer', ['notnull' => true]);
        $table->addForeignKeyConstraint(
            'monsters',
            ['monster_id'],
            ['id'],
            ['onUpdate' => 'CASCADE', 'onDelete' => 'CASCADE'],
            'fk_monster_id'
        );
        $table->setPrimaryKey(['id']);
    }

    /**
     * @param Schema $schema
     * @throws SchemaException
     */
    public function down(Schema $schema) : void
    {
        $table = $schema->getTable('players_monsters');
        $table->removeForeignKey('fk_player_id');
        $table->removeForeignKey('fk_monster_id');

        $schema->dropTable('players_monsters');
    }
}
