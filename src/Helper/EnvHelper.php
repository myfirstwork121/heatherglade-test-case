<?php declare(strict_types=1);

namespace App\Helper;

/**
 * Class EnvHelper
 * @package App\Helper
 */
class EnvHelper
{
    /**
     * @param string $key
     * @return array|bool|false|string|null
     */
    public static function getParam(string $key)
    {
        $value = getenv($key);

        if (!$value) {
            return null;
        }

        switch (strtolower($value)) {
            case 'true':
            case '(true)':
                return true;
            case 'false':
            case '(false)':
                return false;
            case 'empty':
            case '(empty)':
                return '';
            case 'null':
            case '(null)':
                return null;
        }

        if (($valueLength = strlen($value)) > 1 && strpos($value, '"') === 0 && $value[$valueLength - 1] === '"') {
            return substr($value, 1, -1);
        }

        return $value;
    }
}