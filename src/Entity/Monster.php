<?php declare(strict_types=1);

namespace App\Entity;

/**
 * Class Monster
 * @package App\Entity
 */
class Monster implements EntityInterface
{
    /**
     * @var int $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $mongoId
     */
    private $mongoId;

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return Player
     */
    public function setId(int $id): EntityInterface
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Monster
     */
    public function setName(string $name): EntityInterface
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getClassName(): string
    {
        return self::class;
    }

    /**
     * @return string
     */
    public function getMongoId(): ?string
    {
        return $this->mongoId;
    }

    /**
     * @param string $mongoId
     * @return EntityInterface
     */
    public function setMongoId(?string $mongoId): EntityInterface
    {
        $this->mongoId = $mongoId;
        return $this;
    }
}
