<?php declare(strict_types=1);

namespace App\Service;

use App\DataMapper\DataMapperInterface;

/**
 * Class AbstractService
 * @package App\Service
 */
abstract class AbstractService
{
    /**
     * @var DataMapperInterface $dataMapper
     */
    protected $dataMapper;

    /**
     * AbstractService constructor.
     * @param DataMapperInterface $dataMapper
     */
    protected function __construct(DataMapperInterface $dataMapper)
    {
        $this->dataMapper = $dataMapper;
    }

    /**
     * @param array $entities
     * @return array
     */
    protected function toArray(array $entities): array
    {
        $data = [];
        foreach ($entities as $entity) {
            $data[] = $this->dataMapper->toArray($entity);
        }

        return $data;
    }

    /**
     * @param array $rawData
     * @return array
     */
    protected function prepareMongoData(?array $rawData): array
    {
        $data = [];
        foreach ($rawData as $datum) {
            $data[] = $this->dataMapper->setData($datum);
        }

        return $data;
    }
}
