<?php declare(strict_types=1);

namespace App\Service;

use App\DataMapper\PlayerDataMapper;
use App\Repository\PlayerMongoRepository;
use App\Repository\PlayerSqlRepository;
use MongoDB\Model\BSONArray;
use MongoDB\Model\BSONDocument;

/**
 * Class PlayerService
 * @package App\Service
 */
class PlayerService extends AbstractService
{
    /**
     * @var PlayerSqlRepository $repository
     */
    private $repository;

    /**
     * @var PlayerMongoRepository $mongoRepository
     */
    private $mongoRepository;

    /**
     * @var MonsterService $monsterService
     */
    private $monsterService;

    /**
     * PlayerService constructor.
     * @param PlayerSqlRepository $repository
     * @param PlayerMongoRepository $mongoRepository
     * @param PlayerDataMapper $dataMapper
     * @param MonsterService $monsterService
     */
    public function __construct(
        PlayerSqlRepository $repository,
        PlayerMongoRepository $mongoRepository,
        PlayerDataMapper $dataMapper,
        MonsterService $monsterService
    ) {
        parent::__construct($dataMapper);
        $this->repository = $repository;
        $this->mongoRepository = $mongoRepository;
        $this->monsterService = $monsterService;
    }

    /**
     * @return array|null
     */
    public function getAll(): ?array
    {
        $players = $this->repository->fetchAll();
        return $this->toArray($players);
    }

    /**
     * @return array
     */
    public function getAllFromMongo(): array
    {
        $rawPlayer = $this->mongoRepository->fetchAll();
        $players = $this->prepareMongoData($rawPlayer);

        return $this->toArray($players);
    }

    /**
     * @param int $playerId
     * @param int $monsterId
     * @return bool|null
     */
    public function appendMonsters(int $playerId, int $monsterId): ?bool
    {
        if ($this->repository->findRelation($playerId, $monsterId) !== null) {
            return null;
        }

        return $this->repository->addMonster($playerId, $monsterId);
    }

    /**
     * @param string $playerId
     * @param string $monsterName
     */
    public function addMonsterToPlayer(string $playerId, string $monsterName): void
    {
        $rawMonsters = $this->mongoRepository->getMonsters($playerId);
        $monsters = $this->getMonstersName($rawMonsters);
        $monsters[] = [PlayerMongoRepository::COLUMN_NAME => $monsterName];

        $this->mongoRepository->addMonster($playerId, $monsters);
    }

    /**
     * @param string $playerId
     * @return array|null
     */
    public function getMonstersFromMongo(string $playerId): ?array
    {
        $rawMonsters = $this->mongoRepository->getMonsters($playerId);
        return $this->monsterService->getMonstersFromMongo($rawMonsters);
    }

    /**
     * @param BSONArray $monsters
     * @return array
     */
    private function getMonstersName($monsters): array
    {
        if ($monsters instanceof BSONDocument) {
            return [[PlayerMongoRepository::COLUMN_NAME => $monsters->name]];
        }

        $data = [];
        foreach ($monsters as $monster) {
            $data[] = [PlayerMongoRepository::COLUMN_NAME => $monster->name];
        }

        return array_map('unserialize', array_unique(array_map('serialize', $data)));
    }
}
