<?php declare(strict_types=1);

namespace App\DataMapper;

use App\Entity\EntityInterface;
use MongoDB\Model\BSONDocument;

/**
 * Interface DataMapperInterface
 * @package App\DataMapper
 */
interface DataMapperInterface
{
    /**
     * @param BSONDocument $data
     * @return EntityInterface
     */
    public function setData(BSONDocument $data): EntityInterface;

    /**
     * @param EntityInterface $monster
     * @return array
     */
    public function toArray(EntityInterface $monster): array;
}
