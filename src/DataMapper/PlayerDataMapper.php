<?php declare(strict_types=1);

namespace App\DataMapper;

use App\Entity\EntityInterface;
use App\Factory\PlayerFactory;
use App\Repository\RepositoryInterface;
use MongoDB\Model\BSONDocument;

/**
 * Class PlayerDataMapper
 * @package App\DataMapper
 */
class PlayerDataMapper implements DataMapperInterface
{
    /**
     * @var PlayerFactory $playerFactory
     */
    private $playerFactory;

    /**
     * PlayerDataMapper constructor.
     * @param PlayerFactory $playerFactory
     */
    public function __construct(PlayerFactory $playerFactory)
    {
        $this->playerFactory = $playerFactory;
    }

    /**
     * @param BSONDocument $data
     * @return EntityInterface
     */
    public function setData(BSONDocument $data): EntityInterface
    {
        return $this->playerFactory->create()->setName($data->name)
            ->setMongoId($data->_id->__toString());
    }

    /**
     * @param EntityInterface $player
     * @return array
     */
    public function toArray(EntityInterface $player): array
    {
        $id = [RepositoryInterface::COLUMN_ID => $player->getMongoId() ?? $player->getId()];
        return array_merge($id, [RepositoryInterface::COLUMN_NAME => $player->getName()]);
    }
}
