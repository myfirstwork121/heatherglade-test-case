<?php declare(strict_types=1);

namespace App\Controller;

use App\Service\MonsterService;
use App\Service\PlayerService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PlayerController
 * @package App\Controller
 */
class PlayerController extends AbstractController
{
    private const REQUEST_PARAM_MONSTER = 'monster';

    /**
     * @var PlayerService $playerService
     */
    private $playerService;

    /**
     * @var MonsterService $monsterService
     */
    private $monsterService;

    /**
     * PlayerController constructor.
     * @param PlayerService $playerService
     * @param MonsterService $monsterService
     */
    public function __construct(
        PlayerService $playerService,
        MonsterService $monsterService
    ) {
        $this->playerService = $playerService;
        $this->monsterService = $monsterService;
    }

    /**
     * @api {get} /players Request Players information
     * @apiName GetPlayers
     * @apiGroup Player
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "id": "1",
     *       "name": "Doe"
     *     }
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->sendResponseWithData($this->playerService->getAll());
    }

    /**
     * @api {get} /players/:id/monsters Request Player monsters information
     * @apiName GetPlayerMonsters
     * @apiGroup Player
     *
     * @apiParam {Integer} id Player unique ID.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "id": "1",
     *       "name": "Doe"
     *     }
     *
     * @param int $player
     * @return JsonResponse
     */
    public function receiveMonsters(int $player): JsonResponse
    {
        return $this->sendResponseWithData(
            $this->monsterService->getMonsters($player)
        );
    }

    /**
    /**
     * @api {post} /players/:id/monster Request Open monster by player
     * @apiName OpenMonsterByPlayer
     * @apiGroup Player
     *
     * @apiParam {Integer} id Player unique ID.
     * @apiParamExample {json} Request-Example:
     *     {
     *       "monster": 1
     *     }
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *
     * @param Request $request
     * @param int $player
     * @return JsonResponse
     */
    public function openMonster(Request $request, int $player): JsonResponse
    {
        $this->playerService->appendMonsters($player, (int)$request->request->get(self::REQUEST_PARAM_MONSTER));
        return $this->sendResponse(Response::HTTP_OK);
    }
}
