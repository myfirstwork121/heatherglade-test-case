<?php declare(strict_types=1);

namespace App\Factory;

use App\Helper\EnvHelper;
use MongoDB\Client as MongoClient;
use MongoDB\Database as MongoDatabase;

/**
 * Class MongoFactory
 * @package App\Factory
 */
class MongoFactory
{
    private const DNS_PATTERN = 'mongodb://%s%s:%s"';
    private const CREDENTIALS_PATTEN = '%s:%s@';
    private const PARAM_DB_HOST = 'DB_MONGO_HOST';
    private const PARAM_DB_PORT = 'DB_MONGO_PORT';
    private const PARAM_DB_DATABASE = 'DB_MONGO_DATABASE';
    private const PARAM_DB_USERNAME = 'DB_MONGO_USERNAME';
    private const PARAM_DB_PASSWORD = 'DB_MONGO_PASSWORD';

    /**
     * @return MongoDatabase
     */
    public function create(): MongoDatabase
    {
        $dns = sprintf(
            self::DNS_PATTERN,
            $this->getCredentials(),
            EnvHelper::getParam(self::PARAM_DB_HOST),
            EnvHelper::getParam(self::PARAM_DB_PORT)
        );

        return (new MongoClient($dns))->selectDatabase(EnvHelper::getParam(self::PARAM_DB_DATABASE));
    }

    /**
     * @return string|null
     */
    private function getCredentials(): ?string
    {
        $userName = EnvHelper::getParam(self::PARAM_DB_USERNAME);
        if ($userName !== null) {
            return sprintf(self::CREDENTIALS_PATTEN, $userName, EnvHelper::getParam(self::PARAM_DB_PASSWORD));
        }

        return null;
    }
}
