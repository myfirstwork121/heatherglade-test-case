<?php declare(strict_types=1);

namespace App\Factory;

use App\Helper\EnvHelper;
use PDO;

/**
 * Class PDOFactory
 * @package App\Factory
 */
class PDOFactory
{
    private const DNS_PATTERN = 'mysql:host=%s;port=%s;dbname=%s';
    private const PARAM_DB_HOST = 'DB_SQL_HOST';
    private const PARAM_DB_PORT = 'DB_SQL_PORT';
    private const PARAM_DB_DATABASE = 'DB_SQL_DATABASE';
    private const PARAM_DB_USERNAME = 'DB_SQL_USERNAME';
    private const PARAM_DB_PASSWORD = 'DB_SQL_PASSWORD';

    /**
     * @return PDO
     */
    public function create(): PDO
    {
        $dns = sprintf(
            self::DNS_PATTERN,
            EnvHelper::getParam(self::PARAM_DB_HOST),
            EnvHelper::getParam(self::PARAM_DB_PORT),
            EnvHelper::getParam(self::PARAM_DB_DATABASE)
        );

        return new PDO(
            $dns,
            EnvHelper::getParam(self::PARAM_DB_USERNAME),
            EnvHelper::getParam(self::PARAM_DB_PASSWORD)
        );
    }
}
